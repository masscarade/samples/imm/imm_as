/* Automation Studio generated header file */
/* Do not edit ! */

#ifndef _MCBASE_
#define _MCBASE_

#include <bur/plctypes.h>

#ifndef _BUR_PUBLIC
#define _BUR_PUBLIC
#endif
#if defined(_WIN32) || defined(__WINDOWS__) || defined(__MINGW32__) || defined(__CYGWIN__)
#if defined(BUILD_MCBASE) || defined(MCBASE_EXPORTS)
#define MCBASE_DLLAPI __declspec(dllexport)
#else
#define MCBASE_DLLAPI __declspec(dllimport)
#endif
#else /* not Windows */
#define MCBASE_DLLAPI _BUR_PUBLIC
#endif

/** @cond NO */
#ifdef	__cplusplus
#define TYPEDEF_FORWARD_MMO_CLASS(ClassName) \
	namespace Gmc { class ClassName; } \
	typedef ::Gmc::ClassName
#define TYPEDEF_FORWARD_MMO_STRUCT(StructName) \
	namespace Gmc { struct StructName; } \
	typedef ::Gmc::StructName

#ifndef _COMMONIF_MOTIONTYPES_DECLARED_
/**
 * Forward declaration of (internal) C++ - interface types or ...
 */
TYPEDEF_FORWARD_MMO_CLASS(TaskMediator) McInternalFubProcessingType;
TYPEDEF_FORWARD_MMO_CLASS(IControlIf) McInternalControlIfType;
TYPEDEF_FORWARD_MMO_CLASS(IAxisIf) McInternalAxisIfType;
TYPEDEF_FORWARD_MMO_CLASS(IAxesGrpIf) McInternalAxesGroupIfType;
#endif
/**
 *  ... declaration of a comparatively equivalent C structure type.
 */
#else	/*< not C++ >:O */
#if !defined(__STDC__) || defined(__STRICT_ANSI__)
#define TYPEDEF_PSEUDO_MMO_CSTRUCT(StructName, Content) \
	typedef struct StructName Content
#else
#define TYPEDEF_PSEUDO_MMO_CSTRUCT(StructName, Content) \
	struct StructName; typedef struct StructName
#endif
TYPEDEF_PSEUDO_MMO_CSTRUCT(FubProcessing_csub, { long states[2]; }) McInternalFubProcessingType;
TYPEDEF_PSEUDO_MMO_CSTRUCT(McControlIfType_csub, { plcdword vtable; }) McInternalControlIfType;
TYPEDEF_PSEUDO_MMO_CSTRUCT(McAxisIfType_csub, { plcdword vtable; }) McInternalAxisIfType;
TYPEDEF_PSEUDO_MMO_CSTRUCT(McAxesGroupIfType_csub, { plcdword vtable; }) McInternalAxesGroupIfType;
#endif	/*< end plain C */
/** @endcond */

/* Datatypes and datatypes of function blocks */
typedef enum McAxisPLCopenStateEnum
{
	mcAXIS_DISABLED,
	mcAXIS_STANDSTILL,
	mcAXIS_HOMING,
	mcAXIS_STOPPING,
	mcAXIS_DISCRETE_MOTION,
	mcAXIS_CONTINUOUS_MOTION,
	mcAXIS_SYNCHRONIZED_MOTION,
	mcAXIS_ERRORSTOP
} McAxisPLCopenStateEnum;

typedef enum McGroupPLCopenStateEnum
{
	mcGROUP_DISABLED,
	mcGROUP_HOMING,
	mcGROUP_STANDBY,
	mcGROUP_MOVING,
	mcGROUP_STOPPING,
	mcGROUP_ERRORSTOP
} McGroupPLCopenStateEnum;

typedef enum McBufferModeEnum
{
	mcABORTING,
	mcBUFFERED
} McBufferModeEnum;

typedef enum McHomingModeEnum
{
	mcHOMING_DIRECT = 0,
	mcHOMING_SWITCH_GATE,
	mcHOMING_ABSOLUTE_SWITCH,
	mcHOMING_LIMIT_SWITCH = 4,
	mcHOMING_ABSOLUTE,
	mcHOMING_DCM = 7,
	mcHOMING_BLOCK_TORQUE = 9,
	mcHOMING_BLOCK_LAG_ERROR = 10,
	mcHOMING_ABSOLUTE_CORRECTION = 133,
	mcHOMING_DCM_CORRECTION = 135,
	mcHOMING_DEFAULT = 140,
	mcHOMING_INIT,
	mcHOMING_RESTORE_POSITION
} McHomingModeEnum;

typedef enum McIplModeEnum
{
	mcIPLM_DEFAULT,
	mcIPLM_OFF,
	mcIPLM_LINEAR,
	mcIPLM_QUADRATIC,
	mcIPLM_QUADRATIC_NO_OVERSHOOT
} McIplModeEnum;

typedef enum McErrorCmdEnum
{
	mcWARNING_CMD = 0,
	mcERROR_CMD,
	mcERROR_STOP_CMD,
	mcERROR_STOP_CTRL_OFF_CMD,
	mcERROR_V_STOP_CTRL_OFF_CMD,
	mcERROR_COAST_TO_STANDSTILL_CMD,
	mcERROR_INDUCTION_HALT_CMD
} McErrorCmdEnum;

typedef enum McEdgeEnum
{
	mcEDGE_POSITIVE,
	mcEDGE_NEGATIVE
} McEdgeEnum;

typedef enum McNetworkTypeEnum
{
	mcNETWORK_POWERLINK
} McNetworkTypeEnum;

typedef enum McTransitionModeEnum
{
	mcTM_NONE
} McTransitionModeEnum;

typedef enum McExecutionModeEnum
{
	mcEM_IMMEDIATELY
} McExecutionModeEnum;

typedef enum McCoordinateSystemEnum
{
	mcACS = 0,
	mcMCS = 1,
	mcPCS = 2,
	mcSCS1 = 3,
	mcSCS2 = 4,
	mcSCS3 = 5,
	mcSCS4 = 6,
	mcSCS5 = 7,
	mcTCS = 9,
	mcJACS = 100
} McCoordinateSystemEnum;

typedef enum McSwitchEnum
{
	mcSWITCH_OFF,
	mcSWITCH_ON
} McSwitchEnum;

typedef enum McProcessParamModeEnum
{
	mcPPM_READ,
	mcPPM_WRITE,
	mcPPM_LOAD_FROM_CONFIG,
	mcPPM_SAVE_TO_CONFIG
} McProcessParamModeEnum;

typedef enum McProcessConfigModeEnum
{
	mcPCM_LOAD,
	mcPCM_SAVE
} McProcessConfigModeEnum;

typedef enum McCfgTypeEnum
{
	mcCFG_NONE = 0,
	mcCFG_TOOLTBL = 900,
	mcCFG_DYNPARTBL = 1100,
	mcCFG_LIMSET_LIN = 1411,
	mcCFG_LIMSET_ROT = 1412,
	mcCFG_AX = 10000,
	mcCFG_AX_BASE_TYP = 10011,
	mcCFG_AX_MOVE_LIM = 10012,
	mcCFG_AX_FEAT_CAM_AUT_CMN = 10101,
	mcCFG_AX_FEAT_PROF_GEN = 10102,
	mcCFG_ACP_AX = 11000,
	mcCFG_ACP_AX_REF = 11011,
	mcCFG_ACP_MECH_ELM = 11012,
	mcCFG_ACP_ENC_LINK = 11013,
	mcCFG_ACP_CTRL = 11014,
	mcCFG_ACP_HOME = 11015,
	mcCFG_ACP_STOP_REAC = 11016,
	mcCFG_ACP_MOVE_ERR_LIM = 11017,
	mcCFG_ACP_JERK_FLTR = 11018,
	mcCFG_ACP_DIG_IN = 11019,
	mcCFG_ACP_SIM = 11020,
	mcCFG_ACP_AX_FEAT = 11021,
	mcCFG_AX_FEAT_CAM_AUT_ACP = 11101,
	mcCFG_AX_FEAT_CAM_LST = 11102,
	mcCFG_AX_FEAT_A_IN = 11103,
	mcCFG_AXGRP_ADMIN = 20000,
	mcCFG_AXGRP_FEAT_HOME_ORD = 20101,
	mcCFG_AXGRP_FEAT_PWR_ON_ORD = 20102,
	mcCFG_AXGRP_FEAT_EX_SNG_AX = 20103,
	mcCFG_AXGRP_PATHGEN = 21000,
	mcCFG_AXGRP_PATHGEN_BASE_SET = 21013,
	mcCFG_AXGRP_FEAT_PRG = 21101,
	mcCFG_AXGRP_FEAT_COMP = 21102,
	mcCFG_AXGRP_FEAT_CDC = 21103,
	mcCFG_AXGRP_FEAT_FF = 21104,
	mcCFG_AXGRP_FEAT_FRM_HIER_STD = 21105,
	mcCFG_AXGRP_FEAT_FRM_HIER_CUS = 21106,
	mcCFG_AXGRP_FEAT_JOG = 21107,
	mcCFG_AXGRP_FEAT_LAH = 21108,
	mcCFG_AXGRP_FEAT_MFUN = 21109,
	mcCFG_AXGRP_FEAT_MON_ELEM = 21110,
	mcCFG_AXGRP_FEAT_MP_LOG = 21111,
	mcCFG_AXGRP_FEAT_PATH_DEF = 21112,
	mcCFG_AXGRP_FEAT_PRG_SIM = 21113,
	mcCFG_AXGRP_FEAT_SPINDELS = 21114,
	mcCFG_AXGRP_FEAT_TOOL = 21115,
	mcCFG_AXGRP_FEAT_WSM = 21116,
	mcCFG_AXGRP_FEAT_EX_PATH_AX = 21117,
	mcCFG_AXGRP_FEAT_PROBE = 21118,
	mcCFG_AXGRP_FEAT_SIG = 21119,
	mcCFG_AXGRP_FEAT_2D_COMP = 21120,
	mcCFG_AXGRP_FEAT_3D_COMP = 21121,
	mcCFG_AXGRP_FEAT_PATH_PREVIEW = 21122,
	mcCFG_MS_2AX_CNC_XY = 51201,
	mcCFG_MS_2AX_CNC_XZ = 51202,
	mcCFG_MS_2AX_CNC_YZ = 51203,
	mcCFG_MS_3AX_CNC_XYZ = 51301,
	mcCFG_MS_4AX_CNC_XYZB = 51401,
	mcCFG_MS_5AX_CNC_XYZCA = 51504,
	mcCFG_MS_6AX_CNC_ZXYBCA = 51603,
	mcCFG_MS_4AX_SCARA_A = 52041,
	mcCFG_MS_2AX_DELTA_A = 52121,
	mcCFG_MS_3AX_DELTA_A = 52131,
	mcCFG_MS_3AX_DELTA_XZB = 52132,
	mcCFG_MS_4AX_DELTA_A = 52141,
	mcCFG_MS_5AX_DELTA_A = 52151,
	mcCFG_MS_4AX_ROB_A = 52401,
	mcCFG_MS_4AX_ROB_B = 52402,
	mcCFG_MS_5AX_ROB_A = 52501,
	mcCFG_MS_6AX_ROB_A = 52601,
	mcCFG_MS_6AX_ROB_B = 52602
} McCfgTypeEnum;

typedef enum McPTCEnum
{
	mcPTC_CYC_1 = 1
} McPTCEnum;

typedef enum McCfgLocLenUnitEnum
{
	mcCLLU_G_SET = 0,
	mcCLLU_MILL = 5066068,
	mcCLLU_M = 5067858,
	mcCLLU_INCH = 4804168,
	mcCLLU_GEN = -1
} McCfgLocLenUnitEnum;

typedef enum McCfgCntDirEnum
{
	mcCCD_STD = 0,
	mcCCD_INV = 1
} McCfgCntDirEnum;

typedef enum McCfgLocRotUnitEnum
{
	mcCLRU_G_SET = 0,
	mcCLRU_DEG = 17476,
	mcCLRU_GRAD = 4274481,
	mcCLRU_REV = 5059636,
	mcCLRU_GEN = -1
} McCfgLocRotUnitEnum;

typedef enum McCfgLimJerkEnum
{
	mcCLJ_NOT_USE = 0,
	mcCLJ_BASIC = 1,
	mcCLJ_ADV = 2
} McCfgLimJerkEnum;

typedef enum McCfgLimTorqEnum
{
	mcCLT_NOT_USE = 0,
	mcCLT_BASIC = 1,
	mcCLT_ADV = 2
} McCfgLimTorqEnum;

typedef enum McCfgLimForEnum
{
	mcCLF_NOT_USE = 0,
	mcCLF_BASIC = 1,
	mcCLF_ADV = 2
} McCfgLimForEnum;

typedef enum McLSPosEnum
{
	mcLSP_NOT_USE = 0,
	mcLSP_USE = 1
} McLSPosEnum;

typedef enum McLSVelEnum
{
	mcLSV_NOT_USE = 0,
	mcLSV_BASIC = 1,
	mcLSV_ADV = 2
} McLSVelEnum;

typedef enum McLSAccEnum
{
	mcLSA_NOT_USE = 0,
	mcLSA_BASIC = 1,
	mcLSA_ADV = 2
} McLSAccEnum;

typedef enum McLSDecEnum
{
	mcLSD_NOT_USE = 0,
	mcLSD_BASIC = 1,
	mcLSD_ADV = 2
} McLSDecEnum;

typedef struct McAdvMoveCycParType
{
	float Velocity;
	float Acceleration;
	float Deceleration;
	float Jerk;
	McSwitchEnum DisableJoltLimitation;
} McAdvMoveCycParType;

typedef struct McOrientType
{
	unsigned long Type;
	double Angle1;
	double Angle2;
	double Angle3;
} McOrientType;

typedef struct McPosType
{
	double X;
	double Y;
	double Z;
} McPosType;

typedef struct McFrameType
{
	McPosType Pos;
	McOrientType Orient;
} McFrameType;

/*
typedef struct McInternalFubProcessingType
{
	signed long states[2];
} McInternalFubProcessingType;

typedef struct McInternalControlIfType
{
	plcdword vtable;
} McInternalControlIfType;
*/

typedef struct McInternalType
{
	unsigned long ID;
	unsigned long Check;
	unsigned long ParamHash;
	plcword State;
	unsigned short Error;
	McInternalFubProcessingType* Treating;
	unsigned long Memory[14];
	unsigned char Flags;
	McInternalControlIfType* ControlIf;
	signed long SeqNo;
} McInternalType;

#if !(defined(__cplusplus) || defined(_MCBASE_HPP_INCLUDED_))
typedef struct McInternalTwoRefType
{
	unsigned long ID;
	unsigned long Check;
	unsigned long ParamHash;
	plcword State;
	unsigned short Error;
	McInternalFubProcessingType* Treating;
	unsigned long Memory[14];
	unsigned char Flags;
	McInternalControlIfType* ControlIf;
	signed long SeqNo;
	McInternalControlIfType* MaControlIf;
	signed long MaSeqNo;
} McInternalTwoRefType;
#else
typedef struct McInternalTwoRefType : public McInternalType
{
	McInternalControlIfType* MaControlIf;
	long MaSeqNo;
} McInternalTwoRefType;
#endif

typedef struct McInternalMappLinkType
{
	unsigned long Internal[2];
} McInternalMappLinkType;

/*
typedef struct McInternalAxisIfType
{
	plcdword vtable;
} McInternalAxisIfType;

typedef struct McInternalAxesGroupIfType
{
	plcdword vtable;
} McInternalAxesGroupIfType;
*/

typedef struct McExec1InternalType
{
	unsigned short i_serno;
	unsigned short i_state;
	signed long Result;
} McExec1InternalType;

typedef struct McAxisType
{
	McInternalAxisIfType* controlif;
	McInternalMappLinkType mappLinkInternal;
	signed long seqNo;
} McAxisType;

typedef struct McAxesGroupType
{
	McInternalAxesGroupIfType* controlif;
	McInternalMappLinkType mappLinkInternal;
} McAxesGroupType;

typedef struct McGetCoordSystemIdentParType
{
	McAxesGroupType* AxesGroup;
} McGetCoordSystemIdentParType;

typedef struct McProcessParamAdvParType
{
	plcstring Name[251];
} McProcessParamAdvParType;

typedef struct McCfgUnboundedArrayType
{
	unsigned long NumberOfElements;
	unsigned long DataAddress;
	unsigned long NumberOfArrayElements;
} McCfgUnboundedArrayType;

typedef struct McCfgReferenceType
{
	plcstring Name[251];
	McCfgTypeEnum ConfigType;
} McCfgReferenceType;

typedef struct McCfgLimVelBaseType
{
	float Velocity;
} McCfgLimVelBaseType;

typedef struct McCfgLimAccBaseType
{
	float Acceleration;
} McCfgLimAccBaseType;

typedef struct McCfgLimDecBaseType
{
	float Deceleration;
} McCfgLimDecBaseType;

typedef struct McCfgLimJerkBaseType
{
	float Jerk;
} McCfgLimJerkBaseType;

typedef struct McCfgExtLimRefType
{
	McCfgReferenceType LimitReference;
} McCfgExtLimRefType;

typedef struct McCfgLimPosType
{
	double LowerLimit;
	double UpperLimit;
} McCfgLimPosType;

typedef struct McCfgLimVelAdvType
{
	float Positive;
	float Negative;
} McCfgLimVelAdvType;

typedef struct McCfgLimAccAdvType
{
	float Positive;
	float Negative;
} McCfgLimAccAdvType;

typedef struct McCfgLimDecAdvType
{
	float Positive;
	float Negative;
} McCfgLimDecAdvType;

typedef struct McCfgLimJerkBasicType
{
	float Jerk;
} McCfgLimJerkBasicType;

typedef struct McCfgLimJerkAdvType
{
	float AccelerationPositive;
	float AccelerationNegative;
	float DecelerationPositive;
	float DecelerationNegative;
} McCfgLimJerkAdvType;

typedef struct McCfgLimJerkType
{
	McCfgLimJerkEnum Type;
	McCfgLimJerkBasicType Basic;
	McCfgLimJerkAdvType Advanced;
} McCfgLimJerkType;

typedef struct McCfgLimTorqBasicType
{
	float Torque;
} McCfgLimTorqBasicType;

typedef struct McCfgLimTorqAdvType
{
	float AccelerationPositive;
	float AccelerationNegative;
	float DecelerationPositive;
	float DecelerationNegative;
} McCfgLimTorqAdvType;

typedef struct McCfgLimTorqType
{
	McCfgLimTorqEnum Type;
	McCfgLimTorqBasicType Basic;
	McCfgLimTorqAdvType Advanced;
} McCfgLimTorqType;

typedef struct McCfgLimForBasicType
{
	float Force;
} McCfgLimForBasicType;

typedef struct McCfgLimForAdvType
{
	float AccelerationPositive;
	float AccelerationNegative;
	float DecelerationPositive;
	float DecelerationNegative;
} McCfgLimForAdvType;

typedef struct McCfgLimForType
{
	McCfgLimForEnum Type;
	McCfgLimForBasicType Basic;
	McCfgLimForAdvType Advanced;
} McCfgLimForType;

typedef struct McLSPosType
{
	McLSPosEnum Type;
	McCfgLimPosType Used;
} McLSPosType;

typedef struct McLSVelType
{
	McLSVelEnum Type;
	McCfgLimVelBaseType Basic;
	McCfgLimVelAdvType Advanced;
} McLSVelType;

typedef struct McLSAccType
{
	McLSAccEnum Type;
	McCfgLimAccBaseType Basic;
	McCfgLimAccAdvType Advanced;
} McLSAccType;

typedef struct McLSDecType
{
	McLSDecEnum Type;
	McCfgLimDecBaseType Basic;
	McCfgLimDecAdvType Advanced;
} McLSDecType;

typedef struct McCfgLimSetLinType
{
	McLSPosType Position;
	McLSVelType Velocity;
	McLSAccType Acceleration;
	McLSDecType Deceleration;
	McCfgLimJerkType Jerk;
	McCfgLimForType Force;
} McCfgLimSetLinType;

typedef struct McCfgLimSetRotType
{
	McLSPosType Position;
	McLSVelType Velocity;
	McLSAccType Acceleration;
	McLSDecType Deceleration;
	McCfgLimJerkType Jerk;
	McCfgLimTorqType Torque;
} McCfgLimSetRotType;

typedef struct McCfgGearBoxType
{
	signed long Input;
	signed long Output;
} McCfgGearBoxType;

typedef struct McCfgRotToLinTrfType
{
	double ReferenceDistance;
} McCfgRotToLinTrfType;

typedef struct MC_BR_ProcessConfig
{
	/* VAR_INPUT (analog) */
	plcstring Name[251];
	unsigned long DataType;
	unsigned long DataAddress;
	McProcessConfigModeEnum Mode;
	/* VAR_OUTPUT (analog) */
	signed long ErrorID;
	/* VAR (analog) */
	McExec1InternalType Internal;
	/* VAR_INPUT (digital) */
	plcbit Execute;
	/* VAR_OUTPUT (digital) */
	plcbit Done;
	plcbit Busy;
	plcbit Error;
} MC_BR_ProcessConfig_typ;

typedef unsigned long McComponentType;

typedef struct MC_BR_ProcessParam
{
	/* VAR_INPUT (analog) */
	McComponentType Component;
	unsigned long DataType;
	unsigned long DataAddress;
	McProcessParamModeEnum Mode;
	McProcessParamAdvParType AdvancedParameters;
	McExecutionModeEnum ExecutionMode;
	/* VAR_OUTPUT (analog) */
	signed long ErrorID;
	/* VAR (analog) */
	McInternalType Internal;
	/* VAR_INPUT (digital) */
	plcbit Execute;
	/* VAR_OUTPUT (digital) */
	plcbit Done;
	plcbit Busy;
	plcbit Active;
	plcbit Error;
} MC_BR_ProcessParam_typ;

typedef struct MC_BR_GetCoordSystemIdent
{
	/* VAR_INPUT (analog) */
	plcstring CoordSystemName[261];
	McGetCoordSystemIdentParType Parameter;
	/* VAR_OUTPUT (analog) */
	signed long ErrorID;
	unsigned long Ident;
	/* VAR (analog) */
	McInternalType Internal;
	/* VAR_INPUT (digital) */
	plcbit Execute;
	/* VAR_OUTPUT (digital) */
	plcbit Done;
	plcbit Busy;
	plcbit Error;
} MC_BR_GetCoordSystemIdent_typ;

typedef McAxisType McPsmAxisType;

/* Prototyping of functions and function blocks */
#ifdef __cplusplus
extern "C" {
#endif

_BUR_PUBLIC void MC_BR_ProcessConfig(struct MC_BR_ProcessConfig* inst);
_BUR_PUBLIC void MC_BR_ProcessParam(struct MC_BR_ProcessParam* inst);
_BUR_PUBLIC void MC_BR_GetCoordSystemIdent(struct MC_BR_GetCoordSystemIdent* inst);

#ifdef __cplusplus
} // End of C-Linkage
#endif
#endif /* _MCBASE_ */

