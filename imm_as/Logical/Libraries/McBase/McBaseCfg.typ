TYPE
	McCfgTypeEnum :
		(
		mcCFG_NONE := 0,
		mcCFG_TOOLTBL := 900,
		mcCFG_DYNPARTBL := 1100,
		mcCFG_LIMSET_LIN := 1411,
		mcCFG_LIMSET_ROT := 1412,
		mcCFG_AX := 10000,
		mcCFG_AX_BASE_TYP := 10011,
		mcCFG_AX_MOVE_LIM := 10012,
		mcCFG_AX_FEAT_CAM_AUT_CMN := 10101,
		mcCFG_AX_FEAT_PROF_GEN := 10102,
		mcCFG_ACP_AX := 11000,
		mcCFG_ACP_AX_REF := 11011,
		mcCFG_ACP_MECH_ELM := 11012,
		mcCFG_ACP_ENC_LINK := 11013,
		mcCFG_ACP_CTRL := 11014,
		mcCFG_ACP_HOME := 11015,
		mcCFG_ACP_STOP_REAC := 11016,
		mcCFG_ACP_MOVE_ERR_LIM := 11017,
		mcCFG_ACP_JERK_FLTR := 11018,
		mcCFG_ACP_DIG_IN := 11019,
		mcCFG_ACP_SIM := 11020,
		mcCFG_ACP_AX_FEAT := 11021,
		mcCFG_AX_FEAT_CAM_AUT_ACP := 11101,
		mcCFG_AX_FEAT_CAM_LST := 11102,
		mcCFG_AX_FEAT_A_IN := 11103,
		mcCFG_AXGRP_ADMIN := 20000,
		mcCFG_AXGRP_FEAT_HOME_ORD := 20101,
		mcCFG_AXGRP_FEAT_PWR_ON_ORD := 20102,
		mcCFG_AXGRP_FEAT_EX_SNG_AX := 20103,
		mcCFG_AXGRP_PATHGEN := 21000,
		mcCFG_AXGRP_PATHGEN_BASE_SET := 21013,
		mcCFG_AXGRP_FEAT_PRG := 21101,
		mcCFG_AXGRP_FEAT_COMP := 21102,
		mcCFG_AXGRP_FEAT_CDC := 21103,
		mcCFG_AXGRP_FEAT_FF := 21104,
		mcCFG_AXGRP_FEAT_FRM_HIER_STD := 21105,
		mcCFG_AXGRP_FEAT_FRM_HIER_CUS := 21106,
		mcCFG_AXGRP_FEAT_JOG := 21107,
		mcCFG_AXGRP_FEAT_LAH := 21108,
		mcCFG_AXGRP_FEAT_MFUN := 21109,
		mcCFG_AXGRP_FEAT_MON_ELEM := 21110,
		mcCFG_AXGRP_FEAT_MP_LOG := 21111,
		mcCFG_AXGRP_FEAT_PATH_DEF := 21112,
		mcCFG_AXGRP_FEAT_PRG_SIM := 21113,
		mcCFG_AXGRP_FEAT_SPINDELS := 21114,
		mcCFG_AXGRP_FEAT_TOOL := 21115,
		mcCFG_AXGRP_FEAT_WSM := 21116,
		mcCFG_AXGRP_FEAT_EX_PATH_AX := 21117,
		mcCFG_AXGRP_FEAT_PROBE := 21118,
		mcCFG_AXGRP_FEAT_SIG := 21119,
		mcCFG_AXGRP_FEAT_2D_COMP := 21120,
		mcCFG_AXGRP_FEAT_3D_COMP := 21121,
		mcCFG_AXGRP_FEAT_PATH_PREVIEW := 21122,
		mcCFG_MS_2AX_CNC_XY := 51201,
		mcCFG_MS_2AX_CNC_XZ := 51202,
		mcCFG_MS_2AX_CNC_YZ := 51203,
		mcCFG_MS_3AX_CNC_XYZ := 51301,
		mcCFG_MS_4AX_CNC_XYZB := 51401,
		mcCFG_MS_5AX_CNC_XYZCA := 51504,
		mcCFG_MS_6AX_CNC_ZXYBCA := 51603,
		mcCFG_MS_4AX_SCARA_A := 52041,
		mcCFG_MS_2AX_DELTA_A := 52121,
		mcCFG_MS_3AX_DELTA_A := 52131,
		mcCFG_MS_3AX_DELTA_XZB := 52132,
		mcCFG_MS_4AX_DELTA_A := 52141,
		mcCFG_MS_5AX_DELTA_A := 52151,
		mcCFG_MS_4AX_ROB_A := 52401,
		mcCFG_MS_4AX_ROB_B := 52402,
		mcCFG_MS_5AX_ROB_A := 52501,
		mcCFG_MS_6AX_ROB_A := 52601,
		mcCFG_MS_6AX_ROB_B := 52602
		);
	McCfgUnboundedArrayType : STRUCT
		NumberOfElements : UDINT;
		DataAddress : UDINT;
		NumberOfArrayElements : UDINT;
	END_STRUCT;
	McCfgReferenceType : STRUCT
		Name : STRING[250];
		ConfigType : McCfgTypeEnum;
	END_STRUCT;
	McPTCEnum :
		( (*Cyclic task class for command processing*)
		mcPTC_CYC_1 := 1
		);
	McCfgLimVelBaseType : STRUCT (*Type mcAGFPDPLIV_BASIC settings*)
		Velocity : REAL; (*Velocity limit in any movement direction [Measurement units/s]*)
	END_STRUCT;
	McCfgLimAccBaseType : STRUCT (*Type mcAGFPDPLIA_BASIC settings*)
		Acceleration : REAL; (*Acceleration limit in any movement direction [Measurement units/s²]*)
	END_STRUCT;
	McCfgLimDecBaseType : STRUCT (*Type mcAGFPDPLID_BASIC settings*)
		Deceleration : REAL; (*Deceleration limit in any movement direction [Measurement units/s²]*)
	END_STRUCT;
	McCfgLimJerkBaseType : STRUCT (*Type mcAGFPDPLIJ_BASIC settings*)
		Jerk : REAL; (*Jerk limit in any movement direction [Measurement units/s³]*)
	END_STRUCT;
	McCfgExtLimRefType : STRUCT (*Type mcAGFPDPL_EXT settings*)
		LimitReference : McCfgReferenceType; (*Name of the limit reference*)
	END_STRUCT;
	McCfgLocLenUnitEnum :
		( (*Measurement unit for the axis*)
		mcCLLU_G_SET := 0,
		mcCLLU_MILL := 5066068,
		mcCLLU_M := 5067858,
		mcCLLU_INCH := 4804168,
		mcCLLU_GEN := -1
		);
	McCfgCntDirEnum :
		( (*Direction of the axis in which the position value is increasing*)
		mcCCD_STD := 0,
		mcCCD_INV := 1
		);
	McCfgLocRotUnitEnum :
		( (*Measurement unit for the axis*)
		mcCLRU_G_SET := 0,
		mcCLRU_DEG := 17476,
		mcCLRU_GRAD := 4274481,
		mcCLRU_REV := 5059636,
		mcCLRU_GEN := -1
		);
	McCfgLimPosType : STRUCT (*Movement range of the axis via two position boundaries*)
		LowerLimit : LREAL; (*Lower software limit position [Measurement units]*)
		UpperLimit : LREAL; (*Upper software limit position [Measurement units]*)
	END_STRUCT;
	McCfgLimVelAdvType : STRUCT (*Type mcAMLV_ADV settings*)
		Positive : REAL; (*Velocity limit in positive movement direction [Measurement units/s]*)
		Negative : REAL; (*Velocity limit in negative movement direction [Measurement units/s]*)
	END_STRUCT;
	McCfgLimAccAdvType : STRUCT (*Type mcAMLA_ADV settings*)
		Positive : REAL; (*Acceleration limit in positive movement direction [Measurement units/s²]*)
		Negative : REAL; (*Acceleration limit in negative movement direction [Measurement units/s²]*)
	END_STRUCT;
	McCfgLimDecAdvType : STRUCT (*Type mcAMLD_ADV settings*)
		Positive : REAL; (*Deceleration limit in positive movement direction [Measurement units/s²]*)
		Negative : REAL; (*Deceleration limit in negative movement direction [Measurement units/s²]*)
	END_STRUCT;
	McCfgLimJerkEnum :
		( (*Jerk selector setting*)
		mcCLJ_NOT_USE := 0,
		mcCLJ_BASIC := 1,
		mcCLJ_ADV := 2
		);
	McCfgLimJerkBasicType : STRUCT (*Type mcCLJ_BASIC settings*)
		Jerk : REAL; (*Jerk limit in any movement direction [Measurement units/s³]*)
	END_STRUCT;
	McCfgLimJerkAdvType : STRUCT (*Type mcCLJ_ADV settings*)
		AccelerationPositive : REAL; (*Jerk limit in positive movement direction during acceleration [Measurement units/s³]*)
		AccelerationNegative : REAL; (*Jerk limit in negative movement direction during acceleration [Measurement units/s³]*)
		DecelerationPositive : REAL; (*Jerk limit in positive movement direction during deceleration [Measurement units/s³]*)
		DecelerationNegative : REAL; (*Jerk limit in negative movement direction during deceleration [Measurement units/s³]*)
	END_STRUCT;
	McCfgLimJerkType : STRUCT (*Jerk limits*)
		Type : McCfgLimJerkEnum; (*Jerk selector setting*)
		Basic : McCfgLimJerkBasicType; (*Type mcCLJ_BASIC settings*)
		Advanced : McCfgLimJerkAdvType; (*Type mcCLJ_ADV settings*)
	END_STRUCT;
	McCfgLimTorqEnum :
		( (*Torque selector setting*)
		mcCLT_NOT_USE := 0,
		mcCLT_BASIC := 1,
		mcCLT_ADV := 2
		);
	McCfgLimTorqBasicType : STRUCT (*Type mcCLT_BASIC settings*)
		Torque : REAL; (*Torque limit in any movement direction [Nm]*)
	END_STRUCT;
	McCfgLimTorqAdvType : STRUCT (*Type mcCLT_ADV settings*)
		AccelerationPositive : REAL; (*Torque limit in positive movement direction during acceleration [Nm]*)
		AccelerationNegative : REAL; (*Torque limit in negative movement direction during acceleration [Nm]*)
		DecelerationPositive : REAL; (*Torque limit in positive movement direction during deceleration [Nm]*)
		DecelerationNegative : REAL; (*Torque limit in negative movement direction during deceleration [Nm]*)
	END_STRUCT;
	McCfgLimTorqType : STRUCT (*Torque limits*)
		Type : McCfgLimTorqEnum; (*Torque selector setting*)
		Basic : McCfgLimTorqBasicType; (*Type mcCLT_BASIC settings*)
		Advanced : McCfgLimTorqAdvType; (*Type mcCLT_ADV settings*)
	END_STRUCT;
	McCfgLimForEnum :
		( (*Force selector setting*)
		mcCLF_NOT_USE := 0,
		mcCLF_BASIC := 1,
		mcCLF_ADV := 2
		);
	McCfgLimForBasicType : STRUCT (*Type mcCLF_BASIC settings*)
		Force : REAL; (*Force limit in any movement direction [N]*)
	END_STRUCT;
	McCfgLimForAdvType : STRUCT (*Type mcCLF_ADV settings*)
		AccelerationPositive : REAL; (*Force limit in positive movement direction during acceleration [N]*)
		AccelerationNegative : REAL; (*Force limit in negative movement direction during acceleration [N]*)
		DecelerationPositive : REAL; (*Force limit in positive movement direction during deceleration [N]*)
		DecelerationNegative : REAL; (*Force limit in negative movement direction during deceleration [N]*)
	END_STRUCT;
	McCfgLimForType : STRUCT (*Limits for the force of the axis*)
		Type : McCfgLimForEnum; (*Force selector setting*)
		Basic : McCfgLimForBasicType; (*Type mcCLF_BASIC settings*)
		Advanced : McCfgLimForAdvType; (*Type mcCLF_ADV settings*)
	END_STRUCT;
	McLSPosEnum :
		( (*Position selector setting*)
		mcLSP_NOT_USE := 0,
		mcLSP_USE := 1
		);
	McLSPosType : STRUCT (*Movement range of the axis via two position boundaries*)
		Type : McLSPosEnum; (*Position selector setting*)
		Used : McCfgLimPosType; (*Type mcLSP_USE settings*)
	END_STRUCT;
	McLSVelEnum :
		( (*Velocity selector setting*)
		mcLSV_NOT_USE := 0,
		mcLSV_BASIC := 1,
		mcLSV_ADV := 2
		);
	McLSVelType : STRUCT (*Velocity limits*)
		Type : McLSVelEnum; (*Velocity selector setting*)
		Basic : McCfgLimVelBaseType; (*Type mcLSV_BASIC settings*)
		Advanced : McCfgLimVelAdvType; (*Type mcLSV_ADV settings*)
	END_STRUCT;
	McLSAccEnum :
		( (*Acceleration selector setting*)
		mcLSA_NOT_USE := 0,
		mcLSA_BASIC := 1,
		mcLSA_ADV := 2
		);
	McLSAccType : STRUCT (*Acceleration limits*)
		Type : McLSAccEnum; (*Acceleration selector setting*)
		Basic : McCfgLimAccBaseType; (*Type mcLSA_BASIC settings*)
		Advanced : McCfgLimAccAdvType; (*Type mcLSA_ADV settings*)
	END_STRUCT;
	McLSDecEnum :
		( (*Deceleration selector setting*)
		mcLSD_NOT_USE := 0,
		mcLSD_BASIC := 1,
		mcLSD_ADV := 2
		);
	McLSDecType : STRUCT (*Deceleration limits*)
		Type : McLSDecEnum; (*Deceleration selector setting*)
		Basic : McCfgLimDecBaseType; (*Type mcLSD_BASIC settings*)
		Advanced : McCfgLimDecAdvType; (*Type mcLSD_ADV settings*)
	END_STRUCT;
	McCfgLimSetLinType : STRUCT (*Main data type corresponding to McCfgTypeEnum mcCFG_LIMSET_LIN*)
		Position : McLSPosType; (*Movement range of the axis via two position boundaries*)
		Velocity : McLSVelType; (*Velocity limits*)
		Acceleration : McLSAccType; (*Acceleration limits*)
		Deceleration : McLSDecType; (*Deceleration limits*)
		Jerk : McCfgLimJerkType; (*Jerk limits*)
		Force : McCfgLimForType; (*Limits for the force of the axis*)
	END_STRUCT;
	McCfgLimSetRotType : STRUCT (*Main data type corresponding to McCfgTypeEnum mcCFG_LIMSET_ROT*)
		Position : McLSPosType; (*Movement range of the axis via two position boundaries*)
		Velocity : McLSVelType; (*Velocity limits*)
		Acceleration : McLSAccType; (*Acceleration limits*)
		Deceleration : McLSDecType; (*Deceleration limits*)
		Jerk : McCfgLimJerkType; (*Jerk limits*)
		Torque : McCfgLimTorqType; (*Torque limits*)
	END_STRUCT;
	McCfgGearBoxType : STRUCT (*Specifies a gearbox by defining the ratio between a gearbox input and output*)
		Input : DINT; (*Number of rotations on the encoder side [Revolutions]*)
		Output : DINT; (*Number of rotations on the load side which correspond to the number of rotations on the encoder side [Revolutions]*)
	END_STRUCT;
	McCfgRotToLinTrfType : STRUCT (*Specifies a transformation factor between the output of the gear and the actual load movement*)
		ReferenceDistance : LREAL; (*Reference distance which is considered for an axis positioning [Measurement units/Gearbox output revolution]*)
	END_STRUCT;
END_TYPE