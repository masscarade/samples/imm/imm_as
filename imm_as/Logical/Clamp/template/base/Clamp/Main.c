
#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

void _INIT ProgramInit(void)
{
	//dummy assignments to get rid of warnings
	void* pDummy = &CloseCommand;
	pDummy = &OpenCommand;
	pDummy = &AxisState;
	pDummy = &gClampBasic;
	pDummy = &gSequenceCore;
}

void _CYCLIC ProgramCyclic(void)
{
	

}

void _EXIT ProgramExit(void)
{

}

